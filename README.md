<!--
SPDX-FileCopyrightText: 2021-2022 Wiktor Kwapisiewicz <wiktor@metacode.biz>
SPDX-FileCopyrightText: 2022-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Simple (experimental) standalone SSH Agent for OpenPGP cards

[![crates-io-badge](https://img.shields.io/crates/v/openpgp-card-ssh-agent.svg)](https://crates.io/crates/openpgp-card-ssh-agent)
[![status-badge](https://ci.codeberg.org/api/badges/openpgp-card/ssh-agent/status.svg)](https://ci.codeberg.org/openpgp-card/ssh-agent)

This is a simple SSH agent that uses keys on OpenPGP cards. The cards are accessed via PC/SC (e.g. via `pcscd`).

The agent supports RSA 2048/4096, NIST P-256/384/521 and Curve 25519 keys.
The agent doesn't currently persist card identities or PINs.

## Install

```
$ cargo install --locked openpgp-card-ssh-agent
```

## Run

```
$ openpgp-card-ssh-agent -H unix://$XDG_RUNTIME_DIR/ocsa.sock
```

## Use

List the available cards, e.g. with the `opgpcard` tool:

```
$ opgpcard list
Available OpenPGP cards:
 FFFE:01234567
```

If you don't see your card, it's possible that GnuPG is blocking it.
The easiest way to work around this is to unplug and replug the card.

(With recent versions of GnuPG, you might consider configuring scdaemon to access
[PC/SC in shared mode](https://www.gnupg.org/documentation/manuals/gnupg/Scdaemon-Options.html#index-pcsc_002dshared)).

Set the `SSH_AUTH_SOCK` environment variable:

```
$ export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ocsa.sock
```

After freshly starting the agent, you need to add your card(s) to the agent once:

```
$ ssh-add -s FFFE:01234567
```

Enter the User PIN to the card (e.g. `123456`).
The agent will verify the User PIN. If the PIN is OK, it will remember both the card ident and the User PIN.

After adding a card, regular ssh usage backed by the authentication key slot on that card will work:

```
$ ssh <hostname>
```

## Notifications for touch confirmation

This SSH agent uses the [notify-rust](https://crates.io/crates/notify-rust) crate to alert the user if the card requires touch confirmation.

To build without the notification mechanism (not recommended), you can use the `--no-default-features` parameter.

# Credit

This project is based *very* heavily on work by Wiktor Kwapisiewicz <wiktor@metacode.biz>,
at https://gitlab.com/sequoia-pgp/ssh-agent-pks
